[![Built with Spacemacs](https://cdn.rawgit.com/syl20bnr/spacemacs/442d025779da2f62fc86c2082703697714db6514/assets/spacemacs-badge.svg)](http://github.com/syl20bnr/spacemacs)


## À propos
Ceci est la version **org-mode** du fameux guide d'installation archlinux de notre cher [Fred Bezies](https://frederic.bezies.free.fr/blog).

## Motivation
Vous me direz puisque le guide est activement maintenu par l'infatigable Fred, et disponible dans differents formats: odt, pdf, epub et ebook pourquoi se faire ch** à vouloir reinventer la roue?

À cela je réponds:

- Difficulté d'accès régulier à son site à partir de mon pays, *l'Algérie* : notre FAI étatique dépend d'Orange Telecom, qui a cette manie de brider la connexion vers les sites du domaine `free.fr` , je le remercie chaleureusement d'ailleurs pour les emails qu'il m'envoit pour me mettre à jour!

- Pour *accélerer* le processus de production : j'utilise [spacemacs](https://spacemacs.org) pour créer le document en `org-mode` puis l'excellent [pandoc](https://johnmacfarlane.net/pandoc/) pour le convertir aux differents formats, j'y reviendrai plus loin.

- Pour maintenir un dépôt git, en accès public pour automatiser ainsi le *workflow*

- Enfin, pour moi c'est aussi une occasion pour me familiariser avec [spacemacs](https://spacemacs.org) qui est un génial *tweak* d'**emacs**, l'éditeur de texte des barbus!

## Crédits

Tout le crédit revient à [Fred Bezies](https://frederic.bezies.free.fr/blog)
Et pour le citer: 
> Merci à Ewolnux, Xarkam, Frédéric Sierra, Ludovic Riand, Vincent Manillier, Thomas Pawlowski, Quentin Bihet et Igor Milhit pour leurs conseils et remarques.

